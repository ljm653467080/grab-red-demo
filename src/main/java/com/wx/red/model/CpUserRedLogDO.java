package com.wx.red.model;


/**
 * 用户抢红包记录表
 */
public class CpUserRedLogDO implements java.io.Serializable {
	private static final long serialVersionUID = 1L;

	//主键
	private Long id;
	//用户红包id
	private Long userRedId;
	//用户id，抢到红包的用户
	private Long userId;
	//红包金额(元)
	private java.math.BigDecimal money;
	//状态，1未领取，2已领取，3已过期
	private Integer status;
	//创建时间
	private java.util.Date createDate;
	//更新时间
	private java.util.Date updateDate;

	/**
	 * 设置：
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public Long getId() {
		return id;
	}
	/**
	 * 设置：用户红包id
	 */
	public void setUserRedId(Long userRedId) {
		this.userRedId = userRedId;
	}
	/**
	 * 获取：用户红包id
	 */
	public Long getUserRedId() {
		return userRedId;
	}
	/**
	 * 设置：用户id，抢到红包的用户
	 */
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	/**
	 * 获取：用户id，抢到红包的用户
	 */
	public Long getUserId() {
		return userId;
	}
	/**
	 * 设置：红包金额(元)
	 */
	public void setMoney(java.math.BigDecimal money) {
		this.money = money;
	}
	/**
	 * 获取：红包金额(元)
	 */
	public java.math.BigDecimal getMoney() {
		return money;
	}
	/**
	 * 设置：状态，1未领取，2已领取，3已过期
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}
	/**
	 * 获取：状态，1未领取，2已领取，3已过期
	 */
	public Integer getStatus() {
		return status;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreateDate(java.util.Date createDate) {
		this.createDate = createDate;
	}
	/**
	 * 获取：创建时间
	 */
	public java.util.Date getCreateDate() {
		return createDate;
	}
	/**
	 * 设置：更新时间
	 */
	public void setUpdateDate(java.util.Date updateDate) {
		this.updateDate = updateDate;
	}
	/**
	 * 获取：更新时间
	 */
	public java.util.Date getUpdateDate() {
		return updateDate;
	}
}
