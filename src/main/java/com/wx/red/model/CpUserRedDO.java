package com.wx.red.model;


/**
 * 用户发红包记录表
 */
public class CpUserRedDO implements java.io.Serializable {
	private static final long serialVersionUID = 1L;

	//用户红包id
	private Long id;
	// 房间id
	private Long roomId;
	//用户id
	private Long userId;
	//用户在本群昵称
	private String nickName;
	//红包金额(元)
	private java.math.BigDecimal money;
	//状态，1未领取，2已领取完，3已过期
	private Integer status;
	//红包个数
	private Integer count;
	//备注
	private String remark;
	//过期时间
	private java.util.Date expiredTime;
	//创建时间
	private java.util.Date createDate;
	//更新时间
	private java.util.Date updateDate;

	/**
	 * 设置：
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public Long getId() {
		return id;
	}

	public Long getRoomId() {
		return roomId;
	}
	public void setRoomId(Long roomId) {
		this.roomId = roomId;
	}
	/**
	 * 设置：用户id
	 */
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	/**
	 * 获取：用户id
	 */
	public Long getUserId() {
		return userId;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	/**

	 * 设置：红包金额(元)
	 */
	public void setMoney(java.math.BigDecimal money) {
		this.money = money;
	}
	/**
	 * 获取：红包金额(元)
	 */
	public java.math.BigDecimal getMoney() {
		return money;
	}
	/**
	 * 设置：状态，1未领取，2已领取完，3已过期
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}
	/**
	 * 获取：状态，1未领取，2已领取完，3已过期
	 */
	public Integer getStatus() {
		return status;
	}
	/**
	 * 设置：红包个数
	 */
	public void setCount(Integer count) {
		this.count = count;
	}
	/**
	 * 获取：红包个数
	 */
	public Integer getCount() {
		return count;
	}
	/**
	 * 设置：备注
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	/**
	 * 获取：备注
	 */
	public String getRemark() {
		return remark;
	}
	/**
	 * 设置：过期时间
	 */
	public void setExpiredTime(java.util.Date expiredTime) {
		this.expiredTime = expiredTime;
	}
	/**
	 * 获取：过期时间
	 */
	public java.util.Date getExpiredTime() {
		return expiredTime;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreateDate(java.util.Date createDate) {
		this.createDate = createDate;
	}
	/**
	 * 获取：创建时间
	 */
	public java.util.Date getCreateDate() {
		return createDate;
	}
	/**
	 * 设置：更新时间
	 */
	public void setUpdateDate(java.util.Date updateDate) {
		this.updateDate = updateDate;
	}
	/**
	 * 获取：更新时间
	 */
	public java.util.Date getUpdateDate() {
		return updateDate;
	}
}
