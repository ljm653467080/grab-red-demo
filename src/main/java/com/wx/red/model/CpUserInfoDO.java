package com.wx.red.model;


/**
 * 用户基本信息
 */
public class CpUserInfoDO implements java.io.Serializable {
	private static final long serialVersionUID = 1L;

	//主键(用户ID)
	private Long id;
	//用户类型
	private Integer type;
	//用户名
	private String userName;
	//昵称
	private String nickName;
	//手机号前缀（如:86）
	private String mobilePrefix;
	//手机号
	private String mobile;

	//其它用户字段...

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getMobilePrefix() {
		return mobilePrefix;
	}

	public void setMobilePrefix(String mobilePrefix) {
		this.mobilePrefix = mobilePrefix;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
}
