package com.wx.red.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.wx.red.util.RedPacketUtil;
import com.wx.red.model.CpUserRedDO;
import com.wx.red.service.UserRedService;
import com.wx.red.service.RedisService;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import org.springframework.util.StringUtils;


/**
 * 用户红包相关
 * 分布式锁后续可以改成redission来实现
 */
@RestController
@RequestMapping(value = "/user/red")
public class UserRedController {

	@Autowired
	private UserRedService userRedService;

//	@Autowired
	private RedisService redisService;

	/**
	 * 发红包
	 * @param userId 用户id，必填
	 * @param roomId 群id，必填
	 * @param money 红包总金额，必填
	 * @param count 红包个数，必填
	 * @param remark 红包备注，可选（需给默认值）
	 * @return
	 */
	@PostMapping("/sendOutRed")
	public String sendOutRed(Long userId, Long roomId, BigDecimal money, int count,
							 String remark) {
		//相关参数校验, 比如最大红包金额，最大红包数量等参数限制 TODO

		// 拆红包
		BigDecimal sum = money.multiply(new BigDecimal(100));
		int total = sum.intValue();
		List<Integer> list = RedPacketUtil.splitRedPacket(total, count);
		if (list == null) {
			return "拆红包失败";
		}
		//加锁
		String key = com.wx.red.util.RedisConstant.SEND_OUT_RED_KEY + userId;
		boolean flag = redisService.setNx(key, key, 5);
		while (!flag) {
			flag = redisService.setNx(key, key, 5);
		}
		CpUserRedDO userRedDO = userRedWrapper(userId, roomId,
				money, count, remark);
		try {
			userRedService.sendOutRed(userId, userRedDO, list);
		} catch (Exception e) {
			return "发红包错误";
		} finally {
			redisService.delete(key);
		}
		return "发红包成功";
	}

	/**
	 * 抢红包
	 *
	 * @param userId 用户id
	 * @param userRedId 红包id
	 * @return
	 */
	@PostMapping("/grabRed")
	public String grabRed(Long userId, Long userRedId) {
		//相关参数校验，比如剩余红包个数大于0才允许抢红包（红包队列是否为空） TODO


		//加锁，判断是否已经领取过了，已领取状态保存30天
		String userReceivekey = com.wx.red.util.RedisConstant.RECEIVE_RED_KEY + userId + "：" + userRedId;
		boolean flag = redisService.setNx(userReceivekey, String.valueOf(1), 30 * 24 * 60 * 60);
		if (!flag) {
			return "红包已领取";
		}

		// 红包是否过期了
		String expiredKey = com.wx.red.util.RedisConstant.EXPIRED_RED_KEY + userRedId;
		String redListKey = com.wx.red.util.RedisConstant.LIST_RED_KEY + userRedId;
		if (redisService.get(expiredKey) == null) {
			// 红包过期则把无用的键值删除
			redisService.delete(userReceivekey);
			redisService.delete(redListKey);
			return "红包已过期";
		}
		// 从redis队列里面rpop一个红包出来，若队列为空，说明红包全部被领取了
		String redLogIdStr = redisService.rpop(redListKey);
		if (StringUtils.isEmpty(redLogIdStr)) {
			String redStatusKey = com.wx.red.util.RedisConstant.RED_STATUS_KEY + userRedId;
			String redStatus = redisService.get(redStatusKey);
			if (redStatus == null) {
				CpUserRedDO userRedDO = userRedService.get(userRedId);
				if (userRedDO != null) {
					userRedDO.setStatus(com.wx.red.util.RedStatusConstant.RECEIVED_RED_STATUS);//红包已领取完
					userRedDO.setUpdateDate(new Date());
					//更新发红包记录表的红包状态为：已领完
					//userRedService.update(userRedDO);

					redisService.setNx(redStatusKey, String.valueOf(2), 1 * 60 * 60);
				}
			}
			// 红包已经被抢光了
			redisService.delete(userReceivekey);
			return "红包已经被抢光了";
		}

		BigDecimal money;
		try {
			money = userRedService.grabRed(userId, userRedId, Long.valueOf(redLogIdStr));
		} catch (Exception e) {
			redisService.delete(userReceivekey);
			return "抢红包失败";
		}
		return "抢红包成功，红包金额：" + money.toString();
	}

	/**
	 * 封装对象
	 * @param userId
	 * @param roomId
	 * @param money
	 * @param count
	 * @param remark
	 * @return
	 */
	private CpUserRedDO userRedWrapper(Long userId, Long roomId, BigDecimal money, int count,
									   String remark) {
		CpUserRedDO userRedDO = new CpUserRedDO();
		userRedDO.setCount(count);
		userRedDO.setRoomId(roomId);
		userRedDO.setMoney(money);
		userRedDO.setRemark(remark);
		userRedDO.setStatus(com.wx.red.util.RedStatusConstant.DEFAULT_RED_STATUS);//默认未领取
		userRedDO.setUserId(userId);
		java.util.Date now = new Date();
		userRedDO.setCreateDate(now);
		userRedDO.setUpdateDate(now);
		java.util.Calendar calendar = java.util.Calendar.getInstance();
		calendar.setTime(now);
		calendar.set(java.util.Calendar.SECOND, 59);
		calendar.set(java.util.Calendar.MILLISECOND, 0);
		calendar.add(java.util.Calendar.DATE, 1);
		userRedDO.setExpiredTime(calendar.getTime());  //设置红包24小时后过期
		return userRedDO;
	}

}

