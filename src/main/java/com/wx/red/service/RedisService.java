package com.wx.red.service;

/**
 * redis操作
 * @author lfq
 *
 */
public interface RedisService {

	/**
	 * 设置
	 * @param key
	 * @param value
	 */
	void set(String key, String value) ;

	/**
	 * 设置
	 * @param key
	 * @param value
	 * 过期秒数
	 */
	void set(String key, String value, Long second);

     /**
      * 获取
      * @param key
      * @return
      */
     String get(String key) ;

	 /**
	  * 获取分布式锁
	  * @param key
	  * @param value
	  * @param expireSecond 过期时间(秒)
	  * @return
	  */
	 boolean setNx(String key, String value, long expireSecond);

	 /**
	  * 删除
	  * @param key
	  */
	 void delete(String key);

	 /**
	  * 发送通知
	  * @param channel
	  * @param msg
	  */
	 void publish(String channel, String msg);


	/**
	 * hash get hashvalue
	 */
	Object getHashValue(String hashStr, String keyStr);

	/**
	 * hash 操作
	 * @param hashStr hash值
	 * @param key  hash里的key
	 * @return
	 */
	boolean hashHasKey(String hashStr, String key);


	/**
	 * 获取hash里的map
	 */
	java.util.Map<Object,Object> hashEntries(String hashStr);


	/**
	 * hash里存执
	 * @param hashStr hash字符串
	 * @param hashKey hashmap key
	 * @param hashValue hashmap value
	 */
	void hashPut(String hashStr, String hashKey, String hashValue);


	/**
	 * 清空hash
	 * hashSter
	 */

	void deleteHash(String hashStr);
	
	boolean lpush(String key, String value);
	
	String rpop(String key);
}
