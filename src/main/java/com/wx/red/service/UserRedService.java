package com.wx.red.service;

import com.wx.red.model.CpUserRedDO;
import java.math.BigDecimal;
import java.util.List;

public interface UserRedService {

    /**
     * 发红包
     * @param userId
     * @param userRedDO
     * @param list
     * @throws RuntimeException
     */
    void sendOutRed(Long userId, CpUserRedDO userRedDO, List<Integer> list) throws RuntimeException;

    /**
     * 抢红包
     * @param userId
     * @param userRedId
     * @param userRedLogId
     * @return BigDecimal
     */
    BigDecimal grabRed(Long userId, Long userRedId, Long userRedLogId) throws RuntimeException;

    CpUserRedDO get(Long id);
}
