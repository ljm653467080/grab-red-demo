package com.wx.red.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.wx.red.model.CpUserRedDO;
import com.wx.red.model.CpUserRedLogDO;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import com.wx.red.dao.CpUserRedDao;
import com.wx.red.dao.CpUserRedLogDao;

@Service
public class UserRedServiceImpl implements UserRedService {

//    @Autowired
    private CpUserRedDao cpUserRedDao;

//    @Autowired
    private CpUserRedLogDao cpUserRedLogDao;

//    @Autowired
    private RedisService redisService;

    @Override
    @Transactional
    public void sendOutRed(Long userId, CpUserRedDO userRedDO,
                    List<Integer> list) throws RuntimeException {
        // 检查用户资金是否充足等参数校验 TODO

        // 写入userRed发红包记录表
        if (cpUserRedDao.save(userRedDO) <= 0) {
            throw new RuntimeException("插入userRed表失败");
        }

        // 写入userRedLog抢红包记录表
        CpUserRedLogDO userRedLogDO = new CpUserRedLogDO();
        userRedLogDO.setUserId(userId);
        userRedLogDO.setUserRedId(userRedDO.getId());
        userRedLogDO.setStatus(com.wx.red.util.RedStatusConstant.DEFAULT_RED_STATUS);
        userRedLogDO.setCreateDate(new Date());
        BigDecimal d100 = new BigDecimal(100);

        // 把红包分成N个小红包丢进redis队列
        String redListKey = com.wx.red.util.RedisConstant.LIST_RED_KEY + userRedDO.getId();
        for (Integer item : list) {
            Long id = com.wx.red.util.SnowflakeIdWorker.generateId();//雪花算法生成抢红包的唯一id
            userRedLogDO.setId(id);
            userRedLogDO.setMoney(new BigDecimal(item).divide(d100, 2, BigDecimal.ROUND_HALF_UP));
            if (cpUserRedLogDao.save(userRedLogDO) <= 0) { //可以修改成批量写入，提高写入效率
                throw new RuntimeException("写入userRedLog失败");
            }
            redisService.lpush(redListKey, String.valueOf(id));
        }
        // 扣减用户余额，记录资金流水 TODO

        //设置用户发的红包过期缓存，expireSecond后红包失效无法再抢红包
        long expireSecond = (userRedDO.getExpiredTime().getTime() - new Date().getTime())/1000;
        String key = com.wx.red.util.RedisConstant.EXPIRED_RED_KEY + userRedDO.getId();
        redisService.setNx(key, key, expireSecond);

        // 通知发红包消息到客户端进行展示, 也可以是MQ形式，然后ws推送到客户端, 消息格式参考：
        // {"roomId":1, "userRedId":1,"nickName":"张三",
        // "userId":3,"userName":"test"，"money":200,"count":10}
        String jsonObject = "xxxxxx"; //json字符串
        String channel = "user_red_notify";
        redisService.publish(channel, jsonObject);
    }

    @Override
    @Transactional
    public BigDecimal grabRed(Long userId, Long userRedId, Long userRedLogId) throws RuntimeException {
        CpUserRedLogDO userRedLogDO = cpUserRedLogDao.get(userRedLogId);
        userRedLogDO.setStatus(com.wx.red.util.RedStatusConstant.RECEIVED_RED_STATUS);
        if (cpUserRedLogDao.update(userRedLogDO) <= 0) {
            throw new RuntimeException("更新抢红包记录失败");
        }

        //增加用户余额，写入资金流水等 TODO


        // 判断是否还有未抢完的红包，全部抢光就更新发红包记录表的状态为：已领完
        Map param = new HashMap<String, Object>();
        param.put("userRedId", userRedId);
        param.put("status", com.wx.red.util.RedStatusConstant.DEFAULT_RED_STATUS);
        if (cpUserRedLogDao.count(param) <= 0) {
            String redStatusKey = com.wx.red.util.RedisConstant.RED_STATUS_KEY + userRedId;
            CpUserRedDO userRedDO = new CpUserRedDO();
            userRedDO.setId(userRedId);
            userRedDO.setStatus(com.wx.red.util.RedStatusConstant.RECEIVED_RED_STATUS);
            userRedDO.setUpdateDate(new Date());
            cpUserRedDao.update(userRedDO);
            redisService.setNx(redStatusKey, String.valueOf(2), 30);
        }
        return userRedLogDO.getMoney();
    }

    @Override
    public CpUserRedDO get(Long id){
        return cpUserRedDao.get(id);
    }
}
