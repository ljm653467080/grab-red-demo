package com.wx.red.util;

public class RedisConstant {

    /**
     * 存放红包队列key
     */
    public final static String LIST_RED_KEY = "hongbao:list:";

    /**
     * 发红包操作加锁key
     */
    public final static String SEND_OUT_RED_KEY = "send:hongbao:";

    /**
     * 抢红包操作加锁key
     */
    public final static String RECEIVE_RED_KEY = "receive:hongbao:";

    /**
     * 红包过期key
     */
    public final static String EXPIRED_RED_KEY = "hongbao:expired:";

    /**
     * 红包领取状态key
     */
    public final static String RED_STATUS_KEY = "hongbao:status:";
}
