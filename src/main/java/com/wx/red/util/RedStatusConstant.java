package com.wx.red.util;

public class RedStatusConstant {

    /**
     * 未领取
     */
    public final static int DEFAULT_RED_STATUS = 1;
    /**
     * 已领取完
     */
    public final static int RECEIVED_RED_STATUS = 2;
    /**
     * 已过期
     */
    public final static int EXPIRED_RED_STATUS = 3;
}
