package com.wx.red.dao;

import com.wx.red.model.CpUserRedDO;
import java.util.List;
import java.util.Map;

/**
 * 用户发红包记录表
 */
public interface CpUserRedDao {

	CpUserRedDO get(Long id);
	
	List<CpUserRedDO> list(Map<String, Object> map);

	int count(Map<String, Object> map);
	
	int save(CpUserRedDO cpUserRed);
	
	int update(CpUserRedDO cpUserRed);
	
	int remove(Long id);
	
	int batchRemove(Long[] ids);
}
