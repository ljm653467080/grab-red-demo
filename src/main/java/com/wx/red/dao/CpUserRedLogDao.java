package com.wx.red.dao;

import com.wx.red.model.CpUserRedLogDO;
import java.util.List;
import java.util.Map;

/**
 * 用户抢红包记录表
 */
public interface CpUserRedLogDao {

	CpUserRedLogDO get(Long id);
	
	List<CpUserRedLogDO> list(Map<String, Object> map);

	int count(Map<String, Object> map);
	
	int save(CpUserRedLogDO cpUserRedLog);
	
	int update(CpUserRedLogDO cpUserRedLog);
	
	int remove(Long id);
	
	int batchRemove(Long[] ids);
}
